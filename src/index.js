import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

let dialogsArray = [
    {id: 1, name: 'Dima'},
    {id: 2, name: 'Pasha'},
    {id: 3, name: 'Masha'},
    {id: 4, name: 'Li'},
    {id: 5, name: 'Sveta'}
]

let messagesArray = [
    {id: 1, message: 'Hi'},
    {id: 2, message: 'Hello'},
    {id: 3, message: 'yo'},
    {id: 4, message: 'Yes'},
    {id: 5, message: 'Oops'}
]

let postsArray = [
    {id: 1, post: 'Hi, how are you?', likesCount: '12'},
    {id: 2, post: "It's my first post!", likesCount: '8'},
    {id: 3, post: "It's my first post!", likesCount: '7'},
    {id: 4, post: "It's my first post!", likesCount: '4'},
    {id: 5, post: "It's my first post!", likesCount: '5'}
]

ReactDOM.render(
    <React.StrictMode>
        <App messages={messagesArray} dialogs={dialogsArray} posts={postsArray}/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
