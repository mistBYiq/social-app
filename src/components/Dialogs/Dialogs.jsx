import React from "react";
import css from './Dialogs.module.css'
import Message from "./Message/Message";
import DialogItem from "./DialogItem/DialogItem";

const Dialogs = (props) => {
    let dialogsElements = props.dialogsArray.map(
        dialog => <DialogItem name={dialog.name} id={dialog.id}/>
    );

    let messagesElements = props.messagesArray.map(
        message => <Message message={message.message}/>
    );
    return (
        <div className={css.dialogs}>
            <div className={css.dialogsItems}>
                {dialogsElements}
            </div>

            <div className={css.messages}>
                {messagesElements}
            </div>

        </div>
    );
}

export default Dialogs;