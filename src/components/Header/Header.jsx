import React, {Component} from 'react';
import h from './Header.module.css'

const Header = () => {
    return (
        <header className={h.header}>
            <img src='https://www.logodesign.net/images/nature-logo.png'></img>
        </header>
    );
}

export default Header;