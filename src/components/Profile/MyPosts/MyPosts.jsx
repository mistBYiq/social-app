import React, {Component} from 'react';
import Post from './Post/Post';
import css from './MyPosts.module.css';


const MyPosts = (props) => {

    let postsElements = props.postsArray.map(post => <Post message={post.post} likesCounter={post.likesCount}/>);

    return (
        <div className={css.postsBlock}>
            <h3>My post</h3>
            <div>
                <textarea></textarea>
                <button className={css.buttonAdd}>Add New Post</button>
                <button className={css.buttonDel}>Remove</button>
            </div>
            <div className={css.posts}>
                {postsElements}
            </div>
        </div>
    );
}

export default MyPosts;