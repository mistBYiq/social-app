import React, {Component} from 'react';
import MyPosts from './MyPosts/MyPosts.jsx';
import p from './Profile.module.css';

const Profile = (props) => {
    return (
        <div>
            <div className={p.item}>
                <img src='https://wallpaperaccess.com/full/124516.jpg'></img>
            </div>
            <div>
                ava + description
            </div>
            <MyPosts postsArray={props.postsArray}/>
        </div>
    );
}

export default Profile;